#!/usr/bin/env bash
#
# Chuarstation deployment file - RI 12/09/17
# Tested in: Raspbian Stretch Lite 2017-09-07 on Raspberry Pi 3 Model B
#
# NOTE: Script to be run as root
#
##############

#
# VARS
##############

# Main vars
CH_ROOT="/opt/chuarstation"
CH_LOGFOLDER="/tmp/chuardeploy"
CH_LOGFILE="${CH_LOGFOLDER}/chuardeploy.log"
CH_PAUSE="sleep 1" # Used to put dramatic pause between steps
CH_MENU="whiptail --title Chuarstation"
CH_TMPFILE="/tmp/chuartmp"

# Console log colors
CH_LOGCOLOR0="tput sgr0" # Reset modifiers
CH_LOGCOLOR1="tput setaf 3" # 2=Green,3=Yellow
CH_LOGCOLOR2="tput bold" # Bold text

# Other
CH_WGET="/usr/bin/wget --no-verbose --append-output=${CH_LOGFILE}"

# SDL2
CH_SDL2_SRC="https://www.libsdl.org/release/SDL2-2.0.5.tar.gz"
CH_SDL2_CMAKE=""
CH_SDL2_MAKE="-j4"

# sfml-pi
CH_SFMLPI_SRC="https://github.com/mickelson/sfml-pi/archive/master.zip"
CH_SFMLPI_CMAKE="-DSFML_RPI=1 -DEGL_INCLUDE_DIR=/opt/vc/include -DEGL_LIBRARY=/opt/vc/lib/libbrcmEGL.so -DGLES_INCLUDE_DIR=/opt/vc/include -DGLES_LIBRARY=/opt/vc/lib/libbrcmGLESv2.so"
CH_SFMLPI_MAKE="-j4"

# FFmpeg
CH_FFMPEG_SRC="https://github.com/FFmpeg/FFmpeg/archive/master.zip"
CH_FFMPEG_CONFIGURE="--enable-shared  --extra-cflags="-march=armv8-a+crc -mfpu=neon-fp-armv8 -mtune=cortex-a53" --enable-gpl --enable-nonfree --enable-static --enable-omx --enable-omx-rpi --enable-mmal --disable-opencl --disable-xlib"
CH_FFMPEG_MAKE="-j4"

# Attract-Mode
CH_ATTRACT_SRC="https://github.com/mickelson/attract/archive/master.zip"
CH_ATTRACT_MAKE="-j4 USE_GLES=1"

# Retroarch
CH_RA_SRC="https://github.com/libretro/RetroArch/archive/master.zip"
CH_RA_CONFIGURE="--disable-materialui --disable-xmb --enable-sdl2 --disable-systemd --disable-ffmpeg --enable-opengles --disable-x11 --enable-dispmanx --disable-wayland --enable-zlib --disable-oss --disable-pulse --enable-neon --disable-vulkan --disable-vulkan_display --enable-videocore --enable-floathard"
CH_RA_MAKE="-j4"

# Retroarch - Nestopia (NES emulator)
CH_RANESTOPIA_SRC="https://github.com/libretro/nestopia/archive/master.zip"
CH_RANESTOPIA_MAKE="-j4"

# Retroarch - snes9x2010 (SNES emulator)
CH_RASNES9X2010_SRC="https://github.com/libretro/snes9x2010/archive/master.zip"
CH_RASNES9X2010_MAKE="-j4 -f Makefile.libretro"

# Retroarch - picodrive (MasterSystem / Megadrive / MegaCD / 32X emulator)
CH_RAPICODRIVE_SRC="https://github.com/libretro/picodrive/archive/master.zip"
CH_RAPICODRIVE_MAKE="-j4 -f Makefile.libretro"

# Retroarch - stella (Atari 2600 emulator)
CH_RASTELLA_SRC="https://github.com/libretro/stella-libretro/archive/master.zip"
CH_RASTELLA_MAKE="-j4"

# Retroarch - gambatte (Gameboy / Gameboy Color emulator)
CH_RAGAMBATTE_SRC="https://github.com/libretro/gambatte-libretro/archive/master.zip"
CH_RAGAMBATTE_MAKE="-j4 -f Makefile.libretro"

#
# FUNCTIONS
##############

function ch_log {
	# Log to console and to $CH_LOGFILE

	# Set text color
	${CH_LOGCOLOR1}
	echo "$(date "+%R") | $1" | tee -ai ${CH_LOGFILE}
	# Reset color
	${CH_LOGCOLOR0}
}

function ch_gotofolder {
	# Function to create a folder and cd into it
	if [ -d "${1}" ]; then
		echo "Folder already exists: ${1}" >> ${CH_LOGFILE}
	else
		mkdir -p "${1}"
	fi
	cd "${1}"
}

function ch_printusage {
	echo "Script usage:"
	echo -e "  chuardeploy.sh all\tDeploy everything"
	echo -e "  chuardeploy.sh <item>\tInstall specific component. * Not yet implemented *"
	echo -e "  chuardeploy.sh -h\tThis help."
}

function ch_start {
	# First function to be run

	# Clean log files if exist
	if [ -d ${CH_LOGFOLDER} ]; then
		rm -rf "${CH_LOGFOLDER}/*" &> /dev/null
	else
		mkdir -p "${CH_LOGFOLDER}" &> /dev/null
	fi
	> "${CH_LOGFILE}"
	ch_log "Using log folder: ${CH_LOGFOLDER}/"
	ch_log ""
	${CH_PAUSE}

	ch_log ""
	ch_log "CHUARSTATION DEPLOYMENT"
	ch_log ""
	${CH_PAUSE}

	# Clean old builds
	ch_log "Cleaning old builds..."
	rm -rf ${CH_ROOT}/src/* &>> ${CH_LOGFILE}

	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_firstime_raspbian {
	# First time config if installing in a baseline Raspbian - to be run manually
	sudo -s
	passwd
	systemctl enable ssh
	systemctl restart ssh
	rpi-update
	apt update;apt upgrade -y
	reboot
}

function ch_deploy_sysupdate_raspbian {
	# Dependencies and tools in a Raspbian environment
	ch_log "Installing dependency packages from Raspbian repos..."
	apt install -y git vim screen nmap tcpdump cmake fontconfig libasound2-dev &>> ${CH_LOGFILE}
	apt install -y libflac-dev libogg-dev libvorbis-dev libopenal-dev &>> ${CH_LOGFILE}
	apt install -y libjpeg8-dev libfreetype6-dev libudev-dev libraspberrypi-dev &>> ${CH_LOGFILE}
	ch_log "Done"
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_prepare {  
	# Create base directories

	ch_log "Preparing environment..."
	ch_gotofolder ${CH_ROOT}
	mkdir bin etc lib lib/retroarch games misc src &>> ${CH_LOGFILE}

	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_sdl2 {

	ch_log "SDL2 | Starting deployment"

	# Get sources
	ch_log "SDL2 | Getting sources"
	ch_gotofolder "${CH_ROOT}/src/sdl2"
	${CH_WGET} "${CH_SDL2_SRC}"
	mkdir tmp
	tar -xzf *gz -C tmp &>> ${CH_LOGFILE}
	cd tmp/*

	# Prepare cmake environment
	ch_log "SDL2 | cmake"
	ch_gotofolder build
	cmake ../ ${CH_SDL2_CMAKE} &>> ${CH_LOGFILE}.sdl2build

	# Compile
	ch_log "SDL2 | make"
	make ${CH_SDL2_MAKE} &>> ${CH_LOGFILE}.sdl2build

	# Install system-wide
	ch_log "SDL2 | make install"
	make install &>> ${CH_LOGFILE}.sdl2build
	ch_log "SDL2 | ldconfig"
	ldconfig &>> ${CH_LOGFILE}.sdl2build

	# Clean
	cd ${CH_ROOT}
	rm -rf "${CH_ROOT}/src/sdl2"

	ch_log "SDL2 | Deployment finished"
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_sfmlpi { # Mickelson's SFML-PI

	ch_log "sfml-pi | Starting deployment"

	# Get sources
	ch_log "sfml-pi | Getting sources"
	ch_gotofolder "${CH_ROOT}/src/sfml-pi"
	${CH_WGET} "${CH_SFMLPI_SRC}"
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Prepare cmake environment
	ch_log "sfml-pi | cmake"
	ch_gotofolder build
	cmake ../ ${CH_SFMLPI_CMAKE} &>> ${CH_LOGFILE}.sfmlpibuild

	# Compile
	ch_log "sfml-pi | make"
	make ${CH_SFMLPI_MAKE} &>> ${CH_LOGFILE}.sfmlpibuild

	# Install system-wide
	ch_log "sfml-pi | make install"
	make install &>> ${CH_LOGFILE}.sfmlpibuild
	ch_log "sfml-pi | ldconfig"
	ldconfig &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}src/sfml-pi"

	ch_log "sfml-pi | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_ffmpeg {
	
	ch_log "FFmpeg | Starting deployment"

	# Get sources
	ch_log "FFmpeg | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ffmpeg
	${CH_WGET} ${CH_FFMPEG_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Configure
	ch_log "FFmpeg | ./configure"
	./configure ${CH_FFMPEG_CONFIGURE} &>> ${CH_LOGFILE}.ffmpegbuild

	# Build
	ch_log "FFmpeg | make"
	make ${CH_FFMPEG_MAKE} &>> ${CH_LOGFILE}.ffmpegbuild
	
	# Install system-wide
	ch_log "FFmpeg | make install"
	make install &>> ${CH_LOGFILE}.ffmpegbuild
	ch_log "FFmpeg | ldconfig"
	ldconfig &>> ${CH_LOGFILE}.ffmpegbuild

	# Clean
	cd ${CH_ROOT}
	rm -rf ${CH_ROOT}/src/ffmpeg

	ch_log "FFmpeg | Deployment finished"
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_attract {

	ch_log "Attract-Mode | Starting deployment"

	# Get sources
	ch_log "Attract-Mode | Getting sources"
	ch_gotofolder "${CH_ROOT}/src/attract"
	${CH_WGET} ${CH_ATTRACT_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Attract-Mode | make"
	make ${CH_ATTRACT_MAKE} &>> ${CH_LOGFILE}.attractbuild

	# Install
	ch_log "Attract-mode | Install"
	cp -vfa config /usr/local/share/attract &>> ${CH_LOGFILE}
	cp -vf attract ${CH_ROOT}/bin &>> ${CH_LOGFILE}
	#to start for first time append "-f DejaVuSans" when running ./attract

	# Clean
	cd ${CH_ROOT}
	rm -rf "${CH_ROOT}/src/attract"

	ch_log "Attract-mode | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_ra {

	ch_log "Retroarch | Starting deployment"

	# Get sources
	ch_log "Retroarch | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra
	${CH_WGET} ${CH_RA_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Configure
	ch_log "Retroarch | ./configure"
	./configure ${CH_RA_CONFIGURE} &>> ${CH_LOGFILE}.rabuild

	# Build
	ch_log "Retroarch | make"
	make ${CH_RA_MAKE} &>> ${CH_LOGFILE}.rabuild

	# Install
	cp -vf retroarch ${CH_ROOT}/bin/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra"

	ch_log "Retroarch | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_ra_core {
# WORK IN PROGRESS
	ra_core="$1"
	ra_coresrc="$2"
	ra_coremake="$3"
	ra_corefolder="$4"

	ch_log "Retroarch | ${ra_core} | Starting deployment"

	# Get sources
	ch_log "Retroarch | ${ra_core} | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-${ra_core}
	${CH_WGET} ${ra_coresrc}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Retroarch | ${ra_core} | make"
	make ${ra_coremake} &>> ${CH_LOGFILE}.ra${ra_core}build

	# Install
	cp -vf ${ra_core}_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-${ra_core}"

	ch_log "Retroarch | $ra_core | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_ranestopia {

	ch_log "Retroarch | Nestopia | Starting deployment"

	# Get sources
	ch_log "Retroarch | Nestopia | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-nestopia
	${CH_WGET} ${CH_RANESTOPIA_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*/libretro

	# Build
	ch_log "Retroarch | Nestopia | make"
	make ${CH_RANESTOPIA_MAKE} &>> ${CH_LOGFILE}.ranestopiabuild

	# Install
	cp -vf nestopia_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-nestopia"

	ch_log "Retroarch | Nestopia | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_rasnes9x2010 {

	ch_log "Retroarch | snes9x2010 | Starting deployment"

	# Get sources
	ch_log "Retroarch | snes9x2010 | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-snes9x2010
	${CH_WGET} ${CH_RASNES9X2010_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Retroarch | snes9x2010 | make"
	make ${CH_RASNES9X2010_MAKE} &>> ${CH_LOGFILE}.rasnes9x2010build

	# Install
	cp -vf snes9x2010_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-snes9x2010"

	ch_log "Retroarch | snes9x2010 | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_rapicodrive {

	ch_log "Retroarch | picodrive | Starting deployment"

	# Get sources
	ch_log "Retroarch | picodrive | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-picodrive
	${CH_WGET} ${CH_RAPICODRIVE_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Retroarch | picodrive | make"
	make ${CH_RAPICODRIVE_MAKE} &>> ${CH_LOGFILE}.rapicodrivebuild

	# Install
	cp -vf picodrive_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-picodrive"

	ch_log "Retroarch | picodrive | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_rastella {

	ch_log "Retroarch | stella | Starting deployment"

	# Get sources
	ch_log "Retroarch | stella | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-stella
	${CH_WGET} ${CH_RASTELLA_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Retroarch | stella | make"
	make ${CH_RASTELLA_MAKE} &>> ${CH_LOGFILE}.rastellabuild

	# Install
	cp -vf stella_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-stella"

	ch_log "Retroarch | stella | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

function ch_deploy_ragambatte {

	ch_log "Retroarch | gambatte | Starting deployment"

	# Get sources
	ch_log "Retroarch | gambatte | Getting sources"
	ch_gotofolder ${CH_ROOT}/src/ra-gambatte
	${CH_WGET} ${CH_RAGAMBATTE_SRC}
	unzip -o * -d tmp > /dev/null
	cd tmp/*

	# Build
	ch_log "Retroarch | gambatte | make"
	make ${CH_RAGAMBATTE_MAKE} &>> ${CH_LOGFILE}.ragambattebuild

	# Install
	cp -vf gambatte_libretro.so ${CH_ROOT}/lib/retroarch/ &>> ${CH_LOGFILE}

	# Clean
	cd "${CH_ROOT}"
	rm -rf "${CH_ROOT}/src/ra-gambatte"

	ch_log "Retroarch | gambatte | Deployment finished."
	ch_log ""
	${CH_PAUSE}
}

#
# MENU IMPLEMENTATION
##############

# Check arguments for help
#case $1 in
#	"-h"|"--help"|"help")
#	ch_printusage
#	exit 0
#	;;
#esac

# Show main title
#${CH_MENU} --msgbox "Welcome to Chuarstation deploy script!" 0 0

# Show select item menu
${CH_MENU} --checklist "Choose items to deploy/update:" 0 0 0 \
	\
	ch_deploy_sdl2 "Library: SDL2" 0 \
	ch_deploy_sfmlpi "Library: sfml-pi" 0 \
	ch_deploy_ffmpeg "Library: FFmpeg" 0 \
	ch_deploy_attract "Frontend: Attract-Mode" 0 \
	ch_deploy_ra "Emulator: Retroarch Main" 0 \
	ch_deploy_ranestopia "Emulator: Retroarch Nestopia (NES)" 0 \
	ch_deploy_rasnes9x2010 "Emulator: Retroarch snes9x2010 (SNES)" 0 \
	ch_deploy_rapicodrive "Emulator: Retroarch Picodrive (Mastersystem / Megadrive / MegaCD / 32X)" 0 \
	ch_deploy_ragambatte "Emulator: Retroarch Gambatte (Gameboy / Gameboy Color)" 0 \
	ch_deploy_rastella "Emulator: Retroarch Stella (Atari 2600)" 0 \
	\
	--notags --separate-output 2> ${CH_TMPFILE}

# If some option selected
if [ $? -eq 0 ] && [ -s ${CH_TMPFILE} ]; then
	ch_start
	ch_deploy_prepare
	# Run each selected item
	for item in $(cat ${CH_TMPFILE}); do
		$item
	done
else
	echo "No options chosen. Exiting."
fi

# Remove temp file
if [ -f ${CH_TMPFILE} ]; then
	rm -f ${CH_TMPFILE}
fi

exit 0
