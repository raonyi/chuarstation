//
// OlRoom Theme (NES)
// Theme by Spinelli
// A huge thank you to the Ptiwee for doing the code
//

class UserConfig {
	//</ label="List items", help="Number of items in the listbox of games", order=1 />
	//listnum="16";

	//</ label="Spin Time", help="Time of a wheel transition (in milliseconds)", order=2 />
	//spin_ms="200";
}

local config = fe.get_config();

local flw = fe.layout.width;
local flh = fe.layout.height;

//Background
local bg = fe.add_image("bg.png", 0, 0, flw, flh);

// Flyer
local flyer = fe.add_artwork("flyer", flw*0.865, flh*0.49, flw*0.10, flh*0.37);
flyer.preserve_aspect_ratio = true;
flyer.trigger = Transition.EndNavigation;

// Create description board
local function add_desc (descitem,msg,x,y)
{
	local descitem = fe.add_text(msg, flw*x, flh*y, flw*0.27, flh*0.024);
	descitem.set_rgb(155, 155, 155);
	descitem.align = Align.Left;
	descitem.font = "board.otf";
	descitem.rotation = 356.3;
	return descitem;
}
add_desc ("desc1","Full name:"					,0.714	,0.135);
add_desc ("desc2"," [Title]"					,0.715	,0.165);
add_desc ("desc3","Manufacturer: [Manufacturer]",0.716	,0.195);
add_desc ("desc4","Year: [Year]"				,0.717	,0.225);
add_desc ("desc5","Category: [Category]"		,0.718	,0.255);
add_desc ("desc6","Players: [Players]"			,0.719	,0.285);
add_desc ("desc7","Emulator: [Emulator]"		,0.720	,0.315);
add_desc ("desc8","Playtime: [PlayedTime]"		,0.721	,0.345);
add_desc ("desc9","Played: [PlayedCount] times"	,0.722	,0.375);

// Snap
local snap = fe.add_artwork("snap", flw*0.34375, flh*0.3528, flw*0.2172, flh*0.2870);
snap.trigger = Transition.EndNavigation;

// Scanlines
local scanlines = fe.add_image("scanlines.png", flw*0.34375, flh*0.3528, flw*0.2172, flh*0.2870);
scanlines.preserve_aspect_ratio = false;
scanlines.alpha = 130;

// TV Borders
local borders = fe.add_image("borders.png", flw*0.34375, flh*0.3528, flw*0.2172, flh*0.2870);
borders.preserve_aspect_ratio = false;


// Listbox Name
local l = fe.add_listbox( flw*0.015, flh*0.16, flw*0.245, flh*0.66 );
l.rows = 16;
l.charsize = 18;
l.selbg_alpha = 0;
l.set_selbg_rgb( 100, 100, 100 );
l.set_rgb( 175, 175, 175 );
l.set_sel_rgb( 255, 255, 255 );
l.align = Align.Left;
l.sel_style = Style.Bold;
l.font = "prstartk.ttf";

// Wheel icon
local wheel = fe.add_artwork("wheel", flw*0.34, flh*0.095, flw*0.23, flh*0.13);
snap.trigger = Transition.EndNavigation;

// Function to check if file exists
fe.load_module("file");
function file_exist(fullpathfilename) {
	try {file(fullpathfilename, "r" );return true;}catch(e){return false;}
}

// Emulator wheel, if image not found, show display name in text
local displaywheel = FeConfigDirectory+"/menu-art/wheel/"+fe.displays[fe.list.display_index].name+".png";
if ( file_exist(displaywheel) == true )
{
	local displaywheelimg = fe.add_image(displaywheel, flw*0.05, flh*0.02, flw*0.15, flh*0.1);
}
else
{
	local displaytxt = fe.add_text(fe.displays[fe.list.display_index].name, flw*0.03, flh*0.02, flw*0.18, flh*0.1);
	displaytxt.charsize = 26;
	displaytxt.font = "prstartk.ttf";
}

// Show filters
local filters = fe.add_text("Filter: [FilterName]", flw*0.017, flh*0.84, flw*0.23, flh*0.05);
filters.align = Align.Left;
filters.charsize = 14;
filters.set_rgb(255, 255, 255);
filters.font = "prstartk.ttf";

