# chuarstation
Configuration and scripts to run a game emulation environment with a Raspberry and Raspbian  
  
Hardware used:
* Raspberry PI 3 Model B
  
Software used:
* Attract Mode (front-end)
* Retroarch
* Libretro cores
* DosBox-SVN
* Kodi
  
Main dependencies:
* sfml-pi
* SDL 2.0.7
* FFmpeg
