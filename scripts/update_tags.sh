#!/usr/bin/env bash
#
# Script to merge all favourites and tags into one generic list.
#
# Usage:
# - Set $romlistall var with the name of your desired generic romlist.
# - Update vars with attract romlist config dir accordingly to your setup.
# Specify at the bottom all the custom tags you want to manage.
# In Attract Mode, create a display using this romlist, and configuring
# filters with favorites and tags.
#
#
# It uses the .tag files of Attract Mode to manage favorites and tags.
# Note that Favourites in Attract Mode are threated differently than other tags.
# The fav/tags lists will be updated with new games after attract mode reboot
#
#set -x

# Set vars
romlists="/opt/chuarstation/etc/attract/romlists"
tagfolder="/opt/chuarstation/etc/attract/romlists/*" # Use wildcard to select all folders inside $romlists
romlistall="all" # Name of Attract Mode romlist to use as the generic one
tmpfile="/tmp/chuarstation.updates.tmp"

# Auxiliary function to actually make changes
# arg1: new/current tag file
# arg2: old tag file
# arg3: tag files folder
# arg4: Optional. Tag to parse, if not present, parse favorites.
function mergelists {

  ## Get differences between new and old tagfile
  diff <(sort ${2}) <(sort ${1}) | grep ^\> | sed -e 's/^>\ //g' > ${tmpfile}.new # Games in new not in old
  diff <(sort ${1}) <(sort ${2}) | grep ^\> | sed -e 's/^>\ //g' > ${tmpfile}.old # Games in old not in new

  ## Check games removed
  # If there are games in old tagfile not in new tagfile, proceed
  if [ -s "${tmpfile}.old" ]; then
    echo "Deleted games detected in tags."
    # Clean output file
    #sed -i 's/\^>\ //g'  ${tmpfile}.old
    # Loop with all display tagfiles except $romlistall
    find ${3} -mindepth 1 -maxdepth 1 -name "*.tag" | grep -v "/${romlistall}.tag"$ | while read displaytagfile; do
      # Delete removed games in each tag file
      comm -23 ${displaytagfile} ${tmpfile}.old > ${tmpfile}.old2
      cat ${tmpfile}.old2 > ${displaytagfile}
    done
  fi

  ## Keep old version and generate new one
  mv "${1}" "${2}"
  cat ${3}/*.tag | sort -u > ${1}

  ## Check games added directly through $romlistall
  # If there are games in new tagfile not in old tagfile, proceed
  if [ -s "${tmpfile}.new" ]; then
    echo "New games detected in tags."
    # Mark its tag in all romlists where is present
    cat ${tmpfile}.new | while read newgame; do
      # Get romlists having this game
      grep ^"${newgame}"\; ${romlists}/*.txt | cut -d\: -f1 | sed -e 's/\.txt$//g' | while read romlistori; do
        # Adapt filename to modify, if tag or favorite
	if [ "${4}" != "" ] ; then
	  romlistori="${romlistori}/${4}.tag"
        else
	  romlistori="${romlistori}.tag"
	fi
        # Check if game already in romlist tag, if not, add it.
        if ! grep "${newgame}" "${romlistori}" > /dev/null ; then echo "${newgame}" >> "${romlistori}"; fi
      done
    done
  fi
    
}

# Func to launch merge of a tag
function mergetag {
  listname="$1"
  find ${romlists} -maxdepth 1 -type d -exec touch "{}/${listname}.tag" \; # Populate tag folders with $listname.tag
  file1="${romlists}/${romlistall}/${listname}.tag"
  file2="${romlists}/${romlistall}/${listname}.tag.old"
  mergelists "$file1" "$file2" "${romlists}/*" "$listname"
}

# Func to merge favorites
function mergefavs {
  file1="${romlists}/${romlistall}.tag"
  file2="${romlists}/${romlistall}.tag.old"
  mergelists "$file1" "$file2" "$romlists"
}

## START
#
# Update favourites list
mergefavs
# Update tag lists - Add as many as you need
mergetag "Party"

